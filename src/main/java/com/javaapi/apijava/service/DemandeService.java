package com.javaapi.apijava.service;

import com.javaapi.apijava.entity.Demande;
import com.javaapi.apijava.repository.DemandeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DemandeService{

    @Autowired
    private DemandeRepository repository;


    public Demande saveDemande(Demande demande){
        return repository.save(demande);
    }
    public List<Demande> saveDemandes(List<Demande> demandes){
        return repository.saveAll(demandes);
    }

    public List<Demande> getDemandes(List<Demande> demandes){
        return repository.findAll();
    }

    public Demande getDemandeById(int id){
        return repository.findById(id).orElse(null);
    }

    public Demande DemandeService(Demande demande) {
        return demande;
    }
}
