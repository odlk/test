package com.javaapi.apijava.controller;

import com.javaapi.apijava.entity.Demande;
import com.javaapi.apijava.service.DemandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public class DemandeController {

    @Autowired
    private DemandeService service;

    @PostMapping(path = "/add")
    public Demande saveDemande(@RequestBody Demande demande){
        return service.DemandeService(demande);
    }
}
