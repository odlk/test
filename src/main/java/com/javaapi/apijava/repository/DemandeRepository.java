package com.javaapi.apijava.repository;

import com.javaapi.apijava.entity.Demande;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DemandeRepository extends JpaRepository<Demande,Integer> {


}
