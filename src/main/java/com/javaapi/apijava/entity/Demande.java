package com.javaapi.apijava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name= "demande")

public class Demande {
    @Id
    @Generated
    private int id;
    private String sujet;
    private String description;
    private String observation;
    private String datedemande;
    private int iddemandeur;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSujet() {
        return this.sujet;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObservation() {
        return this.observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getDatedemande() {
        return this.datedemande;
    }

    public void setDatedemande(String datedemande) {
        this.datedemande = datedemande;
    }

    public int getIddemandeur() {
        return this.iddemandeur;
    }

    public void setIddemandeur(int iddemandeur) {
        this.iddemandeur = iddemandeur;
    }



}
